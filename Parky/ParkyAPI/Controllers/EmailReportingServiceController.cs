﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MongoDB.Driver;
using ParkyAPI.Data;
using ParkyAPI.Models;
using ParkyAPI.Models.Dtos;
using ParkyAPI.Repository.IRepository;


namespace ParkyAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EmailReportingServiceController : ControllerBase
    {
        private readonly IEmailServiceRepository _emailServiceRepository;
        private readonly IMapper _mapper;

        public EmailReportingServiceController(IEmailServiceRepository nationalParkRepository, IMapper mapper)
        {
            _emailServiceRepository = nationalParkRepository;
            _mapper = mapper;
        }

        [HttpGet("{jobId}")]
        public async Task<IActionResult> GetEmailBulkReportsByJobIdAsync(Guid jobId)
        {
            IAsyncCursor<EmailReportItemDataStore> objList = await _emailServiceRepository.GetEmailBulkReportsByJobIdAsync(jobId);

            var objDto = new List<EmailServiceDto>();

            while (await objList.MoveNextAsync())
            {
                foreach (var current in objList.Current)
                {
                    objDto.Add(_mapper.Map<EmailServiceDto>(current));
                }
            }

            return Ok(objDto);
        }

        [HttpGet("{jobId}/{recipient}")]
        public async Task<IActionResult> GetEmailBulkReportsByRecipientAsync(Guid jobId, string recipient)
        {
            IAsyncCursor<EmailReportItemDataStore> objList = await _emailServiceRepository.GetEmailBulkReportsByRecipientAsync(jobId, recipient);

            var objDto = new List<EmailServiceDto>();

            while (await objList.MoveNextAsync())
            {
                foreach (var current in objList.Current)
                {
                    objDto.Add(_mapper.Map<EmailServiceDto>(current));
                }
            }

            return Ok(objDto);
        }
    }

}